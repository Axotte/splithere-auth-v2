import { SequelizeModuleOptions } from '@nestjs/sequelize';

export const dbConfig: Record<string, SequelizeModuleOptions> = {
  develop: {
    name: 'splithere-db',
    username: 'user',
    password: 'test123',
    dialect: 'sqlite',
    storage: ':memory:',
    autoLoadModels: true,
    synchronize: true,
  },
  test: {
    dialect: 'postgres',
    host: 'db',
    port: 5432,
    username: 'postgres',
    password: 'Test123',
    database: 'auth_db',
    autoLoadModels: true,
    synchronize: true,
    pool: {
      max: 5,
      min: 0,
      idle: 10000,
    },
  },
};
