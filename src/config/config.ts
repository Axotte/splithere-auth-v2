/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { dbConfig } from './dbConfig';
import { Transport } from '@nestjs/microservices';

export default () => ({
  secret: process.env.SECRET || 'develop',
  accessTokenValidTime: process.env.ACCESS_TOKEN_VALID_TIME || 12,
  refreshTokenValidTime: process.env.REFRESH_TOKEN_VALID_TIME || 10080,
  db: dbConfig[process.env.ENV || 'develop'],
  accountMicroserviceOptions: {
    transport: Transport.RMQ,
    options: {
      urls: [process.env.RABBITMQ_URL || 'amqp://localhost:5672'],
      queue: 'account_queue',
    },
  },
});
