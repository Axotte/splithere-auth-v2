import { Injectable, Inject } from '@nestjs/common';
import { Token } from '../database/models/token.model';
import { InjectModel } from '@nestjs/sequelize';
import { UserDto } from '../microservice/dtos/user.dto';
import { ClientProxy, RpcException } from '@nestjs/microservices';
import { Op } from 'sequelize';

@Injectable()
export class TokenAuthorizationService {
  constructor(
    @InjectModel(Token) private readonly token: typeof Token,
    @Inject('ACCOUNT_MICROSERVICE_CLIENT')
    private readonly accountClient: ClientProxy
  ) {}

  async varifyToken(authenticationHeader: string): Promise<UserDto> {
    if (!authenticationHeader.includes('Bearer ')) {
      throw new RpcException('Its not a bearer');
    }

    const tokenString = authenticationHeader.replace('Bearer ', '');
    const token = await this.token.findOne({
      where: {
        accessToken: tokenString,
      },
    });

    if (!token) {
      throw new RpcException('Invalid token');
    }

    if (token.accessTokenExpirationDate.getTime() < new Date().getTime()) {
      throw new RpcException('Token expired');
    }

    return this.accountClient
      .send<UserDto>({ cmd: 'getUser' }, token.ownerUuid)
      .toPromise();
  }

  async deleteTokens(uuids: string[]): Promise<boolean> {
    return this.token
      .destroy({
        where: {
          ownerUuid: {
            [Op.in]: uuids,
          },
        },
      })
      .thenReturn(true);
  }
}
