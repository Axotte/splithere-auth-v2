import {
  Injectable,
  BadRequestException,
  ConflictException,
  UnauthorizedException,
  Inject,
  RequestTimeoutException,
  Logger,
} from '@nestjs/common';
import { Token } from '../database/models/token.model';
import { RegisterDto } from '../validators/register.dto';
import { InjectModel } from '@nestjs/sequelize';
import { hash, compareSync } from 'bcrypt';
import { ConfigService } from '@nestjs/config';
import { encode } from 'jwt-simple';
import { LoginDto } from '../validators/login.dto';
import { RefreshTokenDto } from '../validators/refreshToken.dto';
import { ClientProxy } from '@nestjs/microservices';
import { UserDto } from '../microservice/dtos/user.dto';
import { timeout, catchError } from 'rxjs/operators';
import { TimeoutError, throwError } from 'rxjs';

@Injectable()
export class AuthService {
  private static passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
  private static isPasswordValid = (password: string) =>
    AuthService.passwordRegex.test(password);

  private accessTokenValidTime: number;
  private refreshTokenValidTime: number;
  private secret: string;

  constructor(
    @InjectModel(Token) private readonly token: typeof Token,
    configService: ConfigService,
    @Inject('ACCOUNT_MICROSERVICE_CLIENT')
    private readonly accountClient: ClientProxy
  ) {
    this.accessTokenValidTime = configService.get<number>(
      'accessTokenValidTime'
    );

    this.refreshTokenValidTime = configService.get<number>(
      'refreshTokenValidTime'
    );

    this.secret = configService.get<string>('secret');
  }

  async register(registerDto: RegisterDto): Promise<Token> {
    if (!AuthService.isPasswordValid(registerDto.password)) {
      throw new BadRequestException('Password to weak');
    }

    if (registerDto.password != registerDto.repeatPassword) {
      throw new BadRequestException('Passwords are not the same');
    }

    const user: UserDto = await this.accountClient
      .send<UserDto>({ cmd: 'createUser' }, registerDto.email)
      .pipe(
        timeout(5000),
        catchError((e) => {
          Logger.debug(e);
          if (e instanceof TimeoutError) {
            return throwError(new RequestTimeoutException());
          } else {
            return throwError(new ConflictException(e.message));
          }
        })
      )
      .toPromise();

    const [
      accessToken,
      accessTokenValidUntil,
      refreshToken,
      refreshTokenValidUntil,
    ] = this.getTokens(registerDto.email);

    const newToken = await this.token.create({
      ownerUuid: user.uuid,
      email: registerDto.email,
      password: await hash(registerDto.password, 10),
      accessToken,
      refreshToken,
      accessTokenExpirationDate: accessTokenValidUntil,
      refreshTokenExpirationDate: refreshTokenValidUntil,
    });

    return this.token.findByPk(newToken.ownerUuid);
  }

  async login(loginDto: LoginDto): Promise<Token> {
    const scopedToken = this.token.scope('withPassword');

    const existingToken = await scopedToken.findOne({
      where: {
        email: loginDto.email,
      },
    });

    if (
      !existingToken ||
      !compareSync(loginDto.password, existingToken.password)
    ) {
      throw new BadRequestException('Invalid user or password');
    }

    return this.updateToken(existingToken);
  }

  async refreshToken(refreshTokenDto: RefreshTokenDto): Promise<Token> {
    const existingToken = await this.token.findOne({
      where: {
        refreshToken: refreshTokenDto.refreshToken,
      },
    });

    if (!existingToken) {
      throw new UnauthorizedException('Invalid Token');
    }

    if (
      existingToken.refreshTokenExpirationDate.getTime() < new Date().getTime()
    ) {
      throw new UnauthorizedException('Refresh token already expired');
    }

    return this.updateToken(existingToken);
  }

  private async updateToken(token: Token): Promise<Token> {
    const [
      accessToken,
      accessTokenValidUntil,
      refreshToken,
      refreshTokenValidUntil,
    ] = this.getTokens(token.email);

    const newToken = {
      email: token.email,
      accessToken,
      refreshToken,
      accessTokenExpirationDate: accessTokenValidUntil,
      refreshTokenExpirationDate: refreshTokenValidUntil,
    };

    await this.token.update(
      { ...token, ...newToken },
      {
        where: {
          ownerUuid: token.ownerUuid,
        },
      }
    );

    return this.token.findByPk(token.ownerUuid);
  }

  private getTokens(email: string): [string, Date, string, Date] {
    const now = new Date();

    const accessTokenValidUntil = new Date(
      now.getTime() + this.accessTokenValidTime * 60 * 1000
    );

    const accessTokenPayload = {
      email,
      validUntil: accessTokenValidUntil,
    };

    const refreshTokenValidUntil = new Date(
      now.getTime() + this.refreshTokenValidTime * 60 * 1000
    );

    const refreshTokenPayload = {
      email,
      validUntil: refreshTokenValidUntil,
    };

    return [
      encode(accessTokenPayload, this.secret),
      accessTokenValidUntil,
      encode(refreshTokenPayload, this.secret),
      refreshTokenValidUntil,
    ];
  }
}
