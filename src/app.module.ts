import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import configuration from './config/config';
import { SequelizeModule, SequelizeModuleOptions } from '@nestjs/sequelize';
import { AuthModule } from './api/auth.module';
import { Token } from './database/models/token.model';

@Module({
  imports: [
    ConfigModule.forRoot({ load: [configuration] }),
    SequelizeModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        return {
          ...configService.get<SequelizeModuleOptions>('db'),
          models: [Token],
        };
      },
    }),
    AuthModule,
  ],
})
export class AppModule {}
