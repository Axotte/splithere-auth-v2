import { testValidUser } from './user.consts';

const now = new Date();

export const testValidToken = {
  ownerUuid: testValidUser.uuid,
  email: testValidUser.email,
  accessToken: 'testAccessToken',
  refreshToken: 'testRefreshToken',
  accessTokenExpirationDate: new Date(now.getTime() + 10000),
  refreshTokenExpirationDate: new Date(now.getTime() + 10000),
};

export const testValidTokenWithPassword = {
  ownerUuid: testValidUser.uuid,
  email: testValidUser.email,
  accessToken: 'testAccessToken',
  refreshToken: 'testRefreshToken',
  password: '$2b$10$DZQHtbOdalHQLMdQWRmU9etM2JdoAMtcooMtMVdmBvX7HeYJZntjq',
  accessTokenExpirationDate: new Date(now.getTime() + 10000),
  refreshTokenExpirationDate: new Date(now.getTime() + 10000),
};

export const testExpiredAccessToken = {
  ownerUuid: testValidUser.uuid,
  email: testValidUser.email,
  accessToken: 'testExpiredAccessToken',
  refreshToken: 'testRefreshToken',
  accessTokenExpirationDate: new Date(now.getTime() - 10000),
  refreshTokenExpirationDate: new Date(now.getTime() + 10000),
};

export const testExpiredRefreshToken = {
  ownerUuid: testValidUser.uuid,
  email: testValidUser.email,
  accessToken: 'testExpiredAccessToken',
  refreshToken: 'testExpiredRefreshToken',
  accessTokenExpirationDate: new Date(now.getTime() - 10000),
  refreshTokenExpirationDate: new Date(now.getTime() - 10000),
};
