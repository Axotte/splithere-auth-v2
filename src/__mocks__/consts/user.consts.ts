import { UserDto } from '../../microservice/dtos/user.dto';

export const validEmail = 'validTest@test.com';

export const timeoutEmail = 'timeoutTest@test.com';

export const alreadyUsedEmail = 'alreadyUsed@test.com';

export const testValidUser: UserDto = {
  uuid: '7fa7976c-b416-4c49-b472-c342d5a7980c',
  email: validEmail,
  avatarUrl: null,
  firstName: 'valid',
  lastName: 'user',
  nickname: 'test',
};
