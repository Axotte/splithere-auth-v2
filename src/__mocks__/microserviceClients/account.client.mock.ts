import { ClientProxy, RpcException } from '@nestjs/microservices';
import { mock, when, objectContaining } from 'ts-mockito';
import { UserDto } from '../../microservice/dtos/user.dto';
import { of, throwError } from 'rxjs';
import { delay } from 'rxjs/operators';
import {
  testValidUser,
  alreadyUsedEmail,
  timeoutEmail,
  validEmail,
} from '../consts/user.consts';

export const mockedAccountClient: ClientProxy = mock<ClientProxy>();

when(
  mockedAccountClient.send<UserDto>(
    objectContaining({ cmd: 'createUser' }),
    objectContaining({
      email: validEmail,
    })
  )
).thenReturn(of(testValidUser));

when(
  mockedAccountClient.send<UserDto>(
    objectContaining({ cmd: 'createUser' }),
    objectContaining({
      email: alreadyUsedEmail,
    })
  )
).thenReturn(throwError(new RpcException('User already exist')));

when(
  mockedAccountClient.send<UserDto>(
    objectContaining({ cmd: 'createUser' }),
    objectContaining({
      email: timeoutEmail,
    })
  )
).thenReturn(of(null).pipe(delay(99999)));
