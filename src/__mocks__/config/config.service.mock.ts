import { mock, when } from 'ts-mockito';
import { ConfigService } from '@nestjs/config';

export const mockedConfigService = mock(ConfigService);

when(mockedConfigService.get<number>('accessTokenValidTime')).thenReturn(12);

when(mockedConfigService.get<number>('refreshTokenValidTime')).thenReturn(
  10080
);

when(mockedConfigService.get<string>('secret')).thenReturn('test');
