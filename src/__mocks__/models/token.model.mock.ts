/* eslint-disable @typescript-eslint/no-explicit-any */
import { mock, when, anything, objectContaining } from 'ts-mockito';
import { Token } from '../../database/models/token.model';
import {
  testValidToken,
  testValidTokenWithPassword,
  testExpiredAccessToken,
  testExpiredRefreshToken,
} from '../consts/token.consts';
import { validEmail } from '../consts/user.consts';

export const mockedToken: typeof Token = mock<typeof Token>();

when(mockedToken.scope(anything())).thenReturn(
  (mockedToken as any).__tsmockitoInstance
);

when(mockedToken.findByPk(testValidToken.ownerUuid)).thenResolve(
  testValidToken as Token
);

when(
  mockedToken.findOne(
    objectContaining({
      where: {
        email: validEmail,
      },
    })
  )
).thenResolve(testValidTokenWithPassword as Token);

when(
  mockedToken.findOne(
    objectContaining({
      where: {
        refreshToken: testValidToken.refreshToken,
      },
    })
  )
).thenResolve(testValidToken as Token);

when(
  mockedToken.findOne(
    objectContaining({
      where: {
        refreshToken: testExpiredAccessToken.refreshToken,
      },
    })
  )
).thenResolve(testExpiredAccessToken as Token);

when(
  mockedToken.findOne(
    objectContaining({
      where: {
        refreshToken: testExpiredRefreshToken.refreshToken,
      },
    })
  )
).thenResolve(testExpiredRefreshToken as Token);

when(mockedToken.update(anything(), anything())).thenReturn(
  Promise.resolve(null)
);

when(mockedToken.create(anything())).thenReturn(
  Promise.resolve(testValidToken as Token)
);
