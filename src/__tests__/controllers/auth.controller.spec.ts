import { AuthController } from '../../api/controllers/auth.controller';
import { AuthService } from '../../services/auth.service';
import { instance } from 'ts-mockito';
import { mockedToken } from '../../__mocks__/models/token.model.mock';
import { mockedConfigService } from '../../__mocks__/config/config.service.mock';
import { mockedAccountClient } from '../../__mocks__/microserviceClients/account.client.mock';
import { GrantType } from '../../validators/grantTypes.enum';
import {
  testValidUser,
  validEmail,
  timeoutEmail,
  alreadyUsedEmail,
} from '../../__mocks__/consts/user.consts';
import {
  BadRequestException,
  RequestTimeoutException,
  ConflictException,
  UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  testValidToken,
  testExpiredAccessToken,
  testExpiredRefreshToken,
} from '../../__mocks__/consts/token.consts';

describe('AuthController test', () => {
  let authController: AuthController;
  let authService: AuthService;
  let configService: ConfigService;
  let secret: string;

  beforeAll(async () => {
    configService = instance(mockedConfigService);
    authService = new AuthService(
      instance(mockedToken),
      configService,
      instance(mockedAccountClient)
    );
    authController = new AuthController(authService);

    secret = configService.get<string>('secret');
  });

  test('registration - success', async () => {
    const token = await authController.register({
      email: validEmail,
      grantType: GrantType.PASSWORD,
      password: 'zaq1@WSX',
      repeatPassword: 'zaq1@WSX',
      secret: secret,
    });
    expect(token.ownerUuid).toEqual(testValidUser.uuid);
  });

  test('registration - password too weak', async () => {
    try {
      await authController.register({
        email: validEmail,
        grantType: GrantType.PASSWORD,
        password: '1',
        repeatPassword: '1',
        secret: secret,
      });
    } catch (e) {
      expect(e).toBeInstanceOf(BadRequestException);
      expect((e as BadRequestException).message).toEqual('Password to weak');
    }
  });

  test('registration - passwords different', async () => {
    try {
      await authController.register({
        email: validEmail,
        grantType: GrantType.PASSWORD,
        password: 'zaq1@WSX',
        repeatPassword: 'zaq1@WSXsasas',
        secret: secret,
      });
    } catch (e) {
      expect(e).toBeInstanceOf(BadRequestException);
      expect((e as BadRequestException).message).toEqual(
        'Passwords are not the same'
      );
    }
  });

  test('registration - timeout', async () => {
    try {
      await authController.register({
        email: timeoutEmail,
        grantType: GrantType.PASSWORD,
        password: 'zaq1@WSX',
        repeatPassword: 'zaq1@WSX',
        secret: secret,
      });
    } catch (e) {
      expect(e).toBeInstanceOf(RequestTimeoutException);
    }
  });

  test('registration - user already exist', async () => {
    try {
      await authController.register({
        email: alreadyUsedEmail,
        grantType: GrantType.PASSWORD,
        password: 'zaq1@WSX',
        repeatPassword: 'zaq1@WSX',
        secret: secret,
      });
    } catch (e) {
      expect(e).toBeInstanceOf(ConflictException);
      expect((e as ConflictException).message).toEqual('User already exist');
    }
  });

  test('login - success', async () => {
    const token = await authController.login({
      email: validEmail,
      grantType: GrantType.PASSWORD,
      password: 'zaq1@WSX',
      secret: secret,
    });
    expect(token.ownerUuid).toEqual(testValidUser.uuid);
  });

  test('login - invalid email', async () => {
    try {
      await authController.login({
        email: 'invalidEmail@test.com',
        grantType: GrantType.PASSWORD,
        password: 'zaq1@WSX',
        secret: secret,
      });
    } catch (e) {
      expect(e).toBeInstanceOf(BadRequestException);
      expect((e as BadRequestException).message).toEqual(
        'Invalid user or password'
      );
    }
  });

  test('login - invalid password', async () => {
    try {
      await authController.login({
        email: validEmail,
        grantType: GrantType.PASSWORD,
        password: 'invalidPassword',
        secret: secret,
      });
    } catch (e) {
      expect(e).toBeInstanceOf(BadRequestException);
      expect((e as BadRequestException).message).toEqual(
        'Invalid user or password'
      );
    }
  });

  test('refresh - success', async () => {
    const token = await authController.refreshToken({
      grantType: GrantType.TOKEN,
      secret: secret,
      refreshToken: testValidToken.refreshToken,
    });
    expect(token.ownerUuid).toEqual(testValidUser.uuid);
  });

  test('refresh - invalid token', async () => {
    try {
      await authController.refreshToken({
        grantType: GrantType.TOKEN,
        secret: secret,
        refreshToken: 'invalid refresh token',
      });
    } catch (e) {
      expect(e).toBeInstanceOf(UnauthorizedException);
      expect((e as UnauthorizedException).message).toEqual('Invalid Token');
    }
  });

  test('refresh - invalid token', async () => {
    try {
      await authController.refreshToken({
        grantType: GrantType.TOKEN,
        secret: secret,
        refreshToken: 'invalid refresh token',
      });
    } catch (e) {
      expect(e).toBeInstanceOf(UnauthorizedException);
      expect((e as UnauthorizedException).message).toEqual('Invalid Token');
    }
  });

  test('refresh - success with expired access token', async () => {
    const token = await authController.refreshToken({
      grantType: GrantType.TOKEN,
      secret: secret,
      refreshToken: testExpiredAccessToken.refreshToken,
    });
    expect(token.ownerUuid).toEqual(testValidUser.uuid);
  });

  test('refresh - expired refresh token', async () => {
    try {
      await authController.refreshToken({
        grantType: GrantType.TOKEN,
        secret: secret,
        refreshToken: testExpiredRefreshToken.refreshToken,
      });
    } catch (e) {
      expect(e).toBeInstanceOf(UnauthorizedException);
      expect((e as UnauthorizedException).message).toEqual(
        'Refresh token already expired'
      );
    }
  });
});
