import { Module } from '@nestjs/common';
import { AuthController } from './controllers/auth.controller';
import { AuthService } from '../services/auth.service';
import { ConfigModule } from '@nestjs/config';
import { SequelizeModule } from '@nestjs/sequelize';
import { Token } from '../database/models/token.model';
import { AuthMicroserviceModule } from '../microservice/authMicroservcie.module';

@Module({
  controllers: [AuthController],
  providers: [AuthService],
  imports: [
    ConfigModule,
    SequelizeModule.forFeature([Token]),
    AuthMicroserviceModule,
  ],
})
export class AuthModule {}
