import { Controller, Post, Body } from '@nestjs/common';
import { RegisterDto } from '../../validators/register.dto';
import { Token } from '../../database/models/token.model';
import { AuthService } from '../../services/auth.service';
import { LoginDto } from '../../validators/login.dto';
import { RefreshTokenDto } from '../../validators/refreshToken.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('register')
  register(@Body() registerDto: RegisterDto): Promise<Token> {
    return this.authService.register(registerDto);
  }

  @Post('login')
  login(@Body() loginDto: LoginDto): Promise<Token> {
    return this.authService.login(loginDto);
  }

  @Post('refresh')
  refreshToken(@Body() refreshTokenDto: RefreshTokenDto): Promise<Token> {
    return this.authService.refreshToken(refreshTokenDto);
  }
}
