import {
  Table,
  Model,
  PrimaryKey,
  Scopes,
  Column,
  DataType,
  CreatedAt,
  UpdatedAt,
  Default,
  DefaultScope,
  Unique,
} from 'sequelize-typescript';
import { UUIDV4 } from 'sequelize';

@DefaultScope(() => ({
  attributes: [
    'ownerUuid',
    'email',
    'accessToken',
    'refreshToken',
    'accessTokenExpirationDate',
    'refreshTokenExpirationDate',
  ],
}))
@Scopes(() => ({
  withPassword: () => ({
    attributes: [
      'ownerUuid',
      'email',
      'accessToken',
      'refreshToken',
      'accessTokenExpirationDate',
      'refreshTokenExpirationDate',
      'password',
    ],
  }),
}))
@Table
export class Token extends Model<Token> {
  @PrimaryKey
  @Default(UUIDV4)
  @Column(DataType.UUID)
  ownerUuid: string;

  @Unique
  @Column(DataType.STRING)
  email: string;

  @Column(DataType.STRING)
  password: string;

  @Column(DataType.STRING)
  accessToken: string;

  @Column(DataType.STRING)
  refreshToken: string;

  @Column(DataType.DATE)
  accessTokenExpirationDate: Date;

  @Column(DataType.DATE)
  refreshTokenExpirationDate: Date;

  @CreatedAt
  createdAt: Date;

  @UpdatedAt
  updatedAt: Date;
}
