import { Controller } from '@nestjs/common';
import { Payload, MessagePattern } from '@nestjs/microservices';
import { UserDto } from '../dtos/user.dto';
import { TokenAuthorizationService } from '../../services/tokenAuthorization.service';

@Controller()
export class AuthMicroserviceController {
  constructor(
    private readonly tokenAuthorizationService: TokenAuthorizationService
  ) {}

  @MessagePattern({ cmd: 'authenticate' })
  validateToken(@Payload() authenticationHeader: string): Promise<UserDto> {
    return this.tokenAuthorizationService.varifyToken(authenticationHeader);
  }

  @MessagePattern({ cmd: 'deleteTokens' })
  deleteTokens(@Payload() uuids: string[]): Promise<boolean> {
    return this.tokenAuthorizationService.deleteTokens(uuids);
  }
}
