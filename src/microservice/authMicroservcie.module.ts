import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AuthMicroserviceController } from './controllers/authMicroservice.controller';
import { TokenAuthorizationService } from '../services/tokenAuthorization.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { Token } from '../database/models/token.model';
import { ClientOptions, ClientProxyFactory } from '@nestjs/microservices';

@Module({
  controllers: [AuthMicroserviceController],
  providers: [
    TokenAuthorizationService,
    {
      provide: 'ACCOUNT_MICROSERVICE_CLIENT',
      useFactory: (configService: ConfigService) => {
        const accountMicroserviceOptions = configService.get<ClientOptions>(
          'accountMicroserviceOptions'
        );
        return ClientProxyFactory.create(accountMicroserviceOptions);
      },
      inject: [ConfigService],
    },
  ],
  imports: [ConfigModule, SequelizeModule.forFeature([Token])],
  exports: ['ACCOUNT_MICROSERVICE_CLIENT'],
})
export class AuthMicroserviceModule {}
