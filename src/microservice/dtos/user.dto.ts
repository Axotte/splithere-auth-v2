export interface UserDto {
  uuid: string;
  email: string;
  firstName: string;
  lastName: string;
  nickname: string;
  avatarUrl: string;
}
