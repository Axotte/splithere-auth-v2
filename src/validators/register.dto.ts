import { ClientSecretDto } from './clientSecret.dto';
import { GrantType } from './grantTypes.enum';
import { IsIn, IsEmail, IsNotEmpty } from 'class-validator';

export class RegisterDto extends ClientSecretDto {
  @IsIn([GrantType.PASSWORD])
  grantType: GrantType;

  @IsEmail()
  email: string;

  @IsNotEmpty()
  password: string;

  @IsNotEmpty()
  repeatPassword: string;
}
