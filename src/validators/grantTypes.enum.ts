export enum GrantType {
  TOKEN = 'TOKEN',
  PASSWORD = 'PASSWORD',
}
