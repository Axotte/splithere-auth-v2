import { ClientSecretDto } from './clientSecret.dto';
import { IsIn, IsEmail, IsNotEmpty } from 'class-validator';
import { GrantType } from './grantTypes.enum';

export class LoginDto extends ClientSecretDto {
  @IsIn([GrantType.PASSWORD])
  grantType: GrantType;

  @IsEmail()
  email: string;

  @IsNotEmpty()
  password: string;
}
