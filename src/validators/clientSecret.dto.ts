import { IsNotEmpty } from 'class-validator';

export class ClientSecretDto {
  @IsNotEmpty()
  secret: string;
}
