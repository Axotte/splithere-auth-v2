import { ClientSecretDto } from './clientSecret.dto';
import { GrantType } from './grantTypes.enum';
import { IsJWT, IsIn } from 'class-validator';

export class RefreshTokenDto extends ClientSecretDto {
  @IsJWT()
  refreshToken: string;
  @IsIn([GrantType.TOKEN])
  grantType: GrantType;
}
